# Preact signals in React demo

This project gives a quick demo in using the preact-signals package in a react project. Just clone, install dependencies and run the project using either

* npm run dev
* yarn dev
* pnpm dev
