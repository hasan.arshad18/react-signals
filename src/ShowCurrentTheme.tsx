import { currentTheme } from "./GlobalState";

const Text = () => {
    return <h1 className="head">{currentTheme.value}</h1>
}

export default Text;