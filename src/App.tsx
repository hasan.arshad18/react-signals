import { currentTheme } from "./GlobalState";
import ToggleThemeButton from "./ToggleThemeButton";
import ShowCurrentTheme from "./ShowCurrentTheme";

function App() {
  return (
    <div className={`main ${currentTheme}`}>
      <ShowCurrentTheme />
      <ToggleThemeButton />
    </div>
  );
}

export default App;