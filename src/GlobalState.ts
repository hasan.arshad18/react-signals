import { computed, signal } from "@preact/signals-react";
const theme = signal<'Light' | 'Dark'>('Light');

const currentTheme = computed(() => theme.value);

function toggleTheme(){
  if(currentTheme.value === 'Light')theme.value = 'Dark';
  else theme.value = 'Light';
}

export {currentTheme, toggleTheme};